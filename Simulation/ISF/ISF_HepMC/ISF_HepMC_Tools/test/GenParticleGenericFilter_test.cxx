/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @author Elmar Ritsch <Elmar.Ritsch@cern.ch>
 * @date October, 2016
 * @brief Tests for ISF::GenParticleGenericFilter.
 */

#undef NDEBUG

#include "CxxUtils/checker_macros.h"
ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

// Google Test
#include "gtest/gtest.h"
#include "GoogleTestTools/InitGaudiGoogleTest.h"

// tested AthAlgTool
#include "../src/GenParticleGenericFilter.h"

// Truth related includes
#include "AtlasHepMC/GenParticle.h"
#include "AtlasHepMC/GenVertex.h"
#include "AtlasHepMC/SimpleVector.h"


namespace ISFTesting {

// Test fixture specifically for SimKernelMT AthAlgorithm
class GenParticleGenericFilter_test: public Athena_test::InitGaudiGoogleTest {

protected:
  virtual void SetUp() override {
    // the tested tool
    IAlgTool* tool = nullptr;
    EXPECT_TRUE( toolSvc->retrieveTool("ISF::GenParticleGenericFilter/TestGenParticleGenericFilter", tool).isSuccess() );
    m_filterTool = dynamic_cast<ISF::GenParticleGenericFilter*>(tool);
  }

  virtual void TearDown() override {
    for (size_t refCount = m_filterTool->refCount(); refCount>0; refCount--) {
      StatusCode sc = toolSvc->releaseTool(m_filterTool);
      ASSERT_TRUE( sc.isSuccess() );
    }
  }

  // the tested AthAlgTool
  ISF::GenParticleGenericFilter* m_filterTool = nullptr;

};  // GenParticleGenericFilter_test fixture


// cppcheck-suppress syntaxError
TEST_F(GenParticleGenericFilter_test, allPropertiesUnset_expectParticlePass) {
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

#ifdef HEPMC3
 HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr();
#else
  const HepMC::GenParticle part{};
#endif
  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, minEtaGreaterThanParticleEta_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-1.3").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(150.*M_PI/180.), 0.0, 1.0*cos(150.*M_PI/180.), 1.0); // rho=1, eta=-1.32
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, minEtaSmallerThanParticleEta_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-.9").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(135.*M_PI/180.), 0.0, 1.0*cos(135.*M_PI/180.), 1.0); // rho=1, eta=-0.88
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, maxEtaSmallerThanParticleEta_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "1.3").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(30.*M_PI/180.), 0.0, 1.0*cos(30.*M_PI/180.), 1.0); // rho=1, eta=+1.32
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, maxEtaGreaterThanParticleEta_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "0.9").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(45.*M_PI/180.), 0.0, 1.0*cos(45.*M_PI/180.), 1.0); // rho=1, eta=+0.88
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, particlePositiveEtaWithinEtaRange_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "0.89").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(45.*M_PI/180.), 0.0, 1.0*cos(45.*M_PI/180.), 1.0); // rho=1, eta=+0.88
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, particleNegativeEtaWithinEtaRange_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-0.89").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(135.*M_PI/180.), 0.0, 1.0*cos(135.*M_PI/180.), 1.0); // rho=1, eta=-0.88
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, particlePositiveEtaOutsideEtaRange_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "0.87").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(45.*M_PI/180.), 0.0, 1.0*cos(45.*M_PI/180.), 1.0); // rho=1, eta=+0.88
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, particleNegativeEtaOutsideEtaRange_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-0.87").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(135.*M_PI/180.), 0.0, 1.0*cos(135.*M_PI/180.), 1.0); // rho=1, eta=-0.88
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, minPhiGreaterThanParticlePhi_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinPhi", "-1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*cos(-1.1), 1.0*sin(-1.1), 0.0, 1.0); // rho=1, phi=-1.1
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, maxPhiSmallerThanParticlePhi_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MaxPhi", "1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*cos(1.1), 1.0*sin(1.1), 0.0, 1.0); // rho=1, phi=1.1
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, pdgDoesntMatchParticle_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("ParticlePDG", "[123,34,5678]").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr();
#else
  const HepMC::GenParticle part{};
#endif
  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, pdgMatchesParticle_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("ParticlePDG", "[123,34,5678]").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom(0, 0, 0, 0);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom, /*pdg id=*/34);
#else
  const HepMC::GenParticle part(mom, /*pdg id=*/34);
#endif
  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, lastPdgMatchesParticle_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("ParticlePDG", "[123,34,5678]").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom(0, 0, 0, 0);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom, /*pdg id=*/5678);
#else
  const HepMC::GenParticle part(mom, /*pdg id=*/5678);
#endif
  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, minMomentumSmallerThanParticleMom_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinMom", "100.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(100.1, 0., 0., 0.);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, minMomentumGreaterThanParticleMom_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinMom", "100.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(99.9, 0., 0., 0.);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, maxMomentumGreaterThanParticleMom_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MaxMom", "100.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(0.001, 0.001, 99.9, 0.);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, maxMomentumSmallerThanParticleMom_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MaxMom", "100.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(0.001, 0.001, 100.1, 0.);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, particleMomentumWithinMomRange_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinMom", "100.").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxMom", "1000.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(0.001, 101., 0.001, 0.);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_TRUE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, particleMomentumBelowMomRange_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinMom", "100.").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxMom", "1000.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(0.001, 99.9, 0.001, 0.);
#ifdef HEPMC3
  HepMC::ConstGenParticlePtr part=HepMC::newConstGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, particleMomentumAboveMomRange_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinMom", "100.").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxMom", "1000.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(0.001, 0.001, 1000.1, 0.);
#ifdef HEPMC3
  HepMC::GenParticlePtr part=HepMC::newGenParticlePtr(mom4, /*pdg id=*/11);
#else
  const HepMC::GenParticle part(mom4, /*pdg id=*/11);
#endif

  ASSERT_FALSE( m_filterTool->pass(part) );
}


TEST_F(GenParticleGenericFilter_test, productionVertexInsideApplicableRadius_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MaxApplicableRadius", "10.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector pos(0.001, 0.001, 0.001, 0.);
#ifdef HEPMC3
  HepMC3::GenVertexPtr vtx = HepMC::newGenVertexPtr(pos);
  auto part = HepMC::newGenParticlePtr(); 
  vtx->add_particle_out(part);

  ASSERT_TRUE( m_filterTool->pass(part) );
#else
  HepMC::GenVertex vtx(pos);
  auto part = HepMC::newGenParticlePtr(); // need dynamic allocation as GenVertex takes ownership
  vtx.add_particle_out(part);

  ASSERT_TRUE( m_filterTool->pass(*part) );
#endif
}


TEST_F(GenParticleGenericFilter_test, productionVertexOutsideApplicableRadiusAndUnmetEtaCut_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-1.31").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxApplicableRadius", "10.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(150.*M_PI/180.), 0.0, 1.0*cos(150.*M_PI/180.), 1.0); // rho=1, eta=-1.32
  auto part = HepMC::newGenParticlePtr(mom4, /*pdg id=*/11); // need dynamic allocation as GenVertex takes ownership

  const HepMC::FourVector pos(0.001, 100., 0.001, 0.);
#ifdef HEPMC3
  HepMC3::GenVertexPtr vtx = HepMC::newGenVertexPtr(pos);
  vtx->add_particle_out(part);

  ASSERT_TRUE( m_filterTool->pass(part) );
#else
  HepMC::GenVertex vtx(pos);
  vtx.add_particle_out(part);

  ASSERT_TRUE( m_filterTool->pass(*part) );
#endif
}


TEST_F(GenParticleGenericFilter_test, productionVertexWithinApplicableRadiusAndUnmetEtaCut_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-1.31").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxApplicableRadius", "10.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(1.0*sin(150.*M_PI/180.), 0.0, 1.0*cos(150.*M_PI/180.), 1.0); // rho=1, eta=-1.32
  auto part = HepMC::newGenParticlePtr(mom4, /*pdg id=*/11); // need dynamic allocation as GenVertex takes ownership

  const HepMC::FourVector pos(0.001, 9.9, 0.001, 0.);
#ifdef HEPMC3
  HepMC3::GenVertexPtr vtx = HepMC::newGenVertexPtr(pos);
  vtx->add_particle_out(part);

  ASSERT_FALSE( m_filterTool->pass(part) );
#else
  HepMC::GenVertex vtx(pos);
  vtx.add_particle_out(part);

  ASSERT_FALSE( m_filterTool->pass(*part) );
#endif
}


TEST_F(GenParticleGenericFilter_test, productionVertexFarForwardInsideApplicableRadiusWithUnmetEtaCut_expectNoPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-2.5").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "2.5").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxApplicableRadius", "1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(0.001, 0.001, 1.0, 1.0); // rho=1, eta=inf AV: note the inf treatment differences in HM2/HM3
  auto part = HepMC::newGenParticlePtr(mom4, /*pdg id=*/11); // need dynamic allocation as GenVertex takes ownership

  const HepMC::FourVector pos(0.001, 0.001, 9999., 0.);
#ifdef HEPMC3
  HepMC3::GenVertexPtr vtx = HepMC::newGenVertexPtr(pos);
  vtx->add_particle_out(part);
  ASSERT_FALSE( m_filterTool->pass(part) );
#else
  HepMC::GenVertex vtx(pos);
  vtx.add_particle_out(part);


  ASSERT_FALSE( m_filterTool->pass(*part) );
#endif
}


TEST_F(GenParticleGenericFilter_test, productionVertexFarForwardOutsideApplicableRadiusWithUnmetEtaCut_expectPass) {
  EXPECT_TRUE( m_filterTool->setProperty("MinEta", "-2.5").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxEta", "2.5").isSuccess() );
  EXPECT_TRUE( m_filterTool->setProperty("MaxApplicableRadius", "1.").isSuccess() );
  EXPECT_TRUE( m_filterTool->initialize().isSuccess() );

  const HepMC::FourVector mom4(0.001, 0.001, 1.0, 1.0); // rho=1, eta=inf AV: note the inf treatment differences in HM2/HM3
  auto part = HepMC::newGenParticlePtr(mom4, /*pdg id=*/11); // need dynamic allocation as GenVertex takes ownership

  const HepMC::FourVector pos(1.1, 0.001, 9999., 0.);
#ifdef HEPMC3
  HepMC3::GenVertexPtr vtx = HepMC::newGenVertexPtr(pos);
  vtx->add_particle_out(part);

  ASSERT_TRUE( m_filterTool->pass(part) );
#else
  HepMC::GenVertex vtx(pos);
  vtx.add_particle_out(part);

  ASSERT_TRUE( m_filterTool->pass(*part) );
#endif
}

} // namespace ISFTesting


int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
  // if the above gets stuck forever while trying to finalize Boost stuff
  // inside SGTools, try to use that:
  //  skips proper finalization:
  //std::quick_exit( RUN_ALL_TESTS() );
}
