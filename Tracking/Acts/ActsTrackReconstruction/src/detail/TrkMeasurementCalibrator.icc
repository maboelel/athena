/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "ActsGeometry/ATLASSourceLink.h"

namespace ActsTrk::detail {

  // internal class implementation
  template <std::size_t DIM>
  inline const Trk::LocalParameters& TrkMeasurementCalibrator::MeasurementAdapter::localPosition() const
  {
    assert( m_measurement and DIM == m_measurement->localParameters().dimension());
    return m_measurement->localParameters();
  }

  template <std::size_t DIM>
  inline const Amg::MatrixX& TrkMeasurementCalibrator::MeasurementAdapter::localCovariance() const
  {
    assert( m_measurement and DIM == m_measurement->localParameters().dimension());
    return m_measurement->localCovariance();
  }

  // class implementation
  template <typename trajectory_t>
  void TrkMeasurementCalibrator::calibrate([[maybe_unused]] const Acts::GeometryContext& gctx,
					   [[maybe_unused]] const Acts::CalibrationContext& cctx,
					   const Acts::SourceLink& sl,
					   typename Acts::MultiTrajectory<trajectory_t>::TrackStateProxy trackState) const
  {
    auto sourceLink = sl.template get<ATLASSourceLink>();
    trackState.setUncalibratedSourceLink(Acts::SourceLink{sl});
    assert(sourceLink);
    const Acts::Surface &surface = m_converterTool->trkSurfaceToActsSurface(sourceLink->associatedSurface());
    setStateFromMeasurement<MeasurementAdapter, trajectory_t>(MeasurementAdapter(*sourceLink),
							      surface.bounds().type(),
							      trackState);
  }

} // namespace ActsTrk::detail

