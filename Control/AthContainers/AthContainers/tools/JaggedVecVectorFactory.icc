/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file AthContainers/tools/JaggedVecVectorFactory.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Apr, 2024
 * @brief Factory object that creates vectors using @c AuxTypeVector,
 *        specialized for JaggedVec.
 */


#include "AthContainers/tools/AuxTypeVector.h"
#include "AthContainers/AuxTypeRegistry.h"
#include "AthContainers/exceptions.h"


namespace SG {


/**
 * @brief Create a vector object of this type.
 * @param auxid ID for the variable being created.
 * @param size Initial size of the new vector.
 * @param capacity Initial capacity of the new vector.
 * @param isLinked True if this variable is linked from another one.
 *                 Must be false.
 */
template <class T, class ALLOC>
std::unique_ptr<IAuxTypeVector>
JaggedVecVectorFactory<T, ALLOC>::create (SG::auxid_t auxid,
                                          size_t size, size_t capacity,
                                          [[maybe_unused]] bool isLinked) const
{
  assert (!isLinked);
  const AuxTypeRegistry& r = AuxTypeRegistry::instance();
  auxid_t linked_id = r.linkedVariable (auxid);

  using linkedAlloc = typename std::allocator_traits<ALLOC>::template rebind_alloc<T>;
  auto linkedVec =
    std::make_unique<AuxTypeVector<T, linkedAlloc> > (linked_id, 0, 0, true);
  return std::make_unique<AuxTypeVector_t> (auxid, size, capacity,
                                            std::move (linkedVec));
}


/**
 * @brief Create a vector object of this type from a data blob.
 * @param auxid ID for the variable being created.
 * @param data The vector object.
 * @param linkedVector The interface for another variable linked to this one.
 *                     (We do not take ownership.)
 * @param isPacked If true, @c data is a @c PackedContainer.
 * @param ownFlag If true, the newly-created IAuxTypeVector object
 *                will take ownership of @c data.
 * @param isLinked True if this variable is linked from another one.
 *
 * @c data should be a pointer to a
 * std::vector<SG::JaggedVec<CONT>, ALLOC<...> > object obtained with new.
 * For this method, isPacked and isLinked must both be false.
 */
template <class CONT, class ALLOC>
std::unique_ptr<IAuxTypeVector>
JaggedVecVectorFactory<CONT, ALLOC>::createFromData (SG::auxid_t auxid,
                                                     void* data,
                                                     IAuxTypeVector* linkedVector,
                                                     [[maybe_unused]] bool isPacked,
                                                     bool ownFlag,
                                                     [[maybe_unused]] bool isLinked) const
{
  assert (!isPacked && !isLinked && linkedVector != nullptr);
  using Holder = SG::JaggedVecVectorHolder<CONT, ALLOC>;
  using vector_type = typename Holder::vector_type;
  return std::make_unique<Holder>
    (auxid, reinterpret_cast<vector_type*>(data), linkedVector, ownFlag);
}


/**
 * @brief Copy elements between vectors.
 * @param auxid The aux data item being operated on.
 * @param dst Container for the destination vector.
 * @param dst_index Index of the first destination element in the vector.
 * @param src Container for the source vector.
 * @param src_index Index of the first source element in the vector.
 * @param n Number of elements to copy.
 * @param for_output If true, apply thinning.
 *
 * @c dst and @ src can be either the same or different.
 */
template <class T, class ALLOC>
void JaggedVecVectorFactory<T, ALLOC>::copyImpl (SG::auxid_t auxid,
                                                 AuxVectorData& dst,
                                                 size_t dst_index,
                                                 const AuxVectorData& src,
                                                 size_t src_index,
                                                 size_t n,
                                                 bool for_output) const
{
  if (n == 0) return;

  // Check for overlaps.
  if (&src == &dst) {
    size_t src_end = src_index + n;
    size_t dst_end = dst_index + n;
    if ((src_end > dst_index && src_end <= dst_end) ||
        (dst_end > src_index && dst_end <= src_end))
    {
      // Overlapping copies not implemented for jagged vectors.
      // Talk to core software if this is an issue.
      throwJaggedVecOverlappingCopy();
    }
  }

  vector_value_type* v_dst = reinterpret_cast<vector_value_type*> (dst.getDataArray (auxid));
  const vector_value_type* v_src = &dst==&src ? v_dst : reinterpret_cast<const vector_value_type*> (src.getDataArray (auxid));
  IAuxTypeVector* dst_lv = dst.getStore()->linkedVector (auxid);
  const SG::auxid_t payload_auxid = dst_lv->auxid();
  const AuxTypeRegistry& r = AuxTypeRegistry::instance();
  const IAuxTypeVectorFactory* payload_fac = r.getFactory (payload_auxid);

  const size_t dst_first = v_dst[dst_index].begin(dst_index);
  size_t src_first = v_src[src_index].begin(src_index);
  const size_t src_first_orig = src_first;
  const size_t n_dst = v_dst[dst_index+n-1].end() - dst_first;
  const size_t n_src = v_src[src_index+n-1].end() - src_first;

  // First copy the Elt elements.  We'll fix up the indices below.
  std::copy_n (v_src+src_index, n, v_dst+dst_index);

  // Adjust the size of the destination payload container.
  if (n_src != n_dst) {
    if (!dst_lv->shift (dst_first+n_dst, n_src - n_dst)) {
      dst.clearCache (payload_auxid);
    }
    if (&dst == &src && src_first > dst_first) {
      src_first += (n_src - n_dst);
    }
  }

  // Copy the payload elements.
  if (for_output) {
    payload_fac->copyForOutput (payload_auxid, dst, dst_first, src, src_first, n_src);
  }
  else {
    payload_fac->copy (payload_auxid, dst, dst_first, src, src_first, n_src);
  }

  // Fixup the Elt entries --- first the ones we copied, then the following
  // ones in the destination container.
  std::for_each_n (v_dst+dst_index, n, Shift (dst_first - src_first_orig));
  std::for_each (v_dst+dst_index+n, v_dst+dst.size_v(), Shift (n_src - n_dst));
}


/**
 * @brief Copy elements between vectors.
 * @param auxid The aux data item being operated on.
 * @param dst Container for the destination vector.
 * @param dst_index Index of the first destination element in the vector.
 * @param src Container for the source vector.
 * @param src_index Index of the first source element in the vector.
 * @param n Number of elements to copy.
 *
 * @c dst and @ src can be either the same or different.
 */
template <class T, class ALLOC>
void JaggedVecVectorFactory<T, ALLOC>::copy (SG::auxid_t auxid,
                                             AuxVectorData& dst,
                                             size_t dst_index,
                                             const AuxVectorData& src,
                                             size_t src_index,
                                             size_t n) const
{
  copyImpl (auxid, dst, dst_index, src, src_index, n, false);
}


/**
 * @brief Copy elements between vectors, possibly applying thinning.
 * @param auxid The aux data item being operated on.
 * @param dst Container for the destination vector.
 * @param dst_index Index of the first destination element in the vector.
 * @param src Container for the source vector.
 * @param src_index Index of source element in the vector.
 * @param src_index Index of the first source element in the vector.
 * @param n Number of elements to copy.
 *
 * @c dst and @ src can be either the same or different.
 */
template <class CONT, class ALLOC>
void JaggedVecVectorFactory<CONT, ALLOC>::copyForOutput
   (SG::auxid_t auxid,
    AuxVectorData& dst,        size_t dst_index,
    const AuxVectorData& src,  size_t src_index,
    size_t n) const
{
  copyImpl (auxid, dst, dst_index, src, src_index, n, true);
}


/**
 * @brief Swap elements between vectors.
 * @param auxid The aux data item being operated on.
 * @param a Container for the first vector.
 * @param aindex Index of the first element in the first vector.
 * @param b Container for the second vector.
 * @param bindex Index of the first element in the second vector.
 * @param n Number of elements to swap.
 *
 * @c a and @ b can be either the same or different.
 * However, the ranges should not overlap.
 */
template <class T, class ALLOC>
void JaggedVecVectorFactory<T, ALLOC>::swap (SG::auxid_t auxid,
                                             AuxVectorData& a, size_t aindex,
                                             AuxVectorData& b, size_t bindex,
                                             size_t n) const
{
  if (n == 0) return;

  vector_value_type* v_a = reinterpret_cast<vector_value_type*> (a.getDataArray (auxid));
  vector_value_type* v_b = &a==&b ? v_a : reinterpret_cast<vector_value_type*> (b.getDataArray (auxid));
  IAuxTypeVector* alv = a.getStore()->linkedVector (auxid);
  IAuxTypeVector* blv = &a==&b ? alv : b.getStore()->linkedVector (auxid);
  const SG::auxid_t payload_auxid = alv->auxid();
  const AuxTypeRegistry& r = AuxTypeRegistry::instance();
  const IAuxTypeVectorFactory* payload_fac = r.getFactory (payload_auxid);

  size_t a_first = v_a[aindex].begin(aindex);
  size_t b_first = v_b[bindex].begin(bindex);
  const size_t a_first_orig = a_first;
  const size_t b_first_orig = b_first;
  const size_t n_a = v_a[aindex+n-1].end() - a_first;
  const size_t n_b = v_b[bindex+n-1].end() - b_first;

  // First swap the Elt entries.  We'll fix up the indices below.
  std::swap_ranges (v_a+aindex, v_a+aindex+n, v_b+bindex);

  // We have two payload ranges to swap, a and b.
  // One is probably longer than the other, so we'll need to adjust the
  // payload vector lengths.
  // But first, swap the common part of the payloads.
  const size_t n_common = std::min (n_a, n_b);
  payload_fac->swap (payload_auxid, a, a_first, b, b_first, n_common);

  // Now we move the tail; that is, the piece of one payload range
  // that is not in the other.  Define a function to reduce duplicate
  // code.  Here, 1 is the range that is longer and 2 the range
  // that is shorter.
  auto shiftTail = [payload_fac, n_common, payload_auxid]
    (AuxVectorData& vd1,
     IAuxTypeVector* lv1,
     size_t& first1,
     const size_t n1,
     AuxVectorData& vd2,
     IAuxTypeVector* lv2,
     size_t& first2)
  {
    // Lengthen the shorter payload to receive the extra elements from
    // the longer one.
    if (!lv2->shift (first2 + n_common, n1 - n_common)) {
      vd2.clearCache (payload_auxid);
    }
    if (&vd1 == &vd2 && first1 > first2) {
      // Special case for self-swapping: keep indices consistent.
      first1 += (n1 - n_common);
    }
    // Copy the extra elements.
    payload_fac->copy (payload_auxid,
                       vd2, first2 + n_common, vd1, first1 + n_common,
                       n1 - n_common);
    // Now remove those elements from the longer one.
    lv1->shift (first1 + n1, - (n1 - n_common));
    if (&vd1 == &vd2 && first2 > first1) {
      // Special case for self-swapping: keep indices consistent.
      first2 -= (n1 - n_common);
    }
  };

  // Now move the tail, depending on which is larger.
  if (n_a > n_b) {
    shiftTail (a, alv, a_first, n_a, b, blv, b_first);
  }
  else if (n_b > n_a) {
    shiftTail (b, blv, b_first, n_b, a, alv, a_first);
  }

  // Now adjust the indices in the Elt ranges that were swapped.
  // In the case of self-swapping, the _first indices may have changed,
  // so need to remember to use the original values.
  std::for_each_n (v_a+aindex, n, Shift (a_first - b_first_orig));
  std::for_each_n (v_b+bindex, n, Shift (b_first - a_first_orig));

  if (n_a != n_b) {
    if (&a == &b) {
      // Self-swapping case.
      // Adjust the indices between the two ranges that were swapped
      // (but we needn't do anything if the number of payload items
      // was the same).
      if (aindex < bindex) {
        std::for_each (v_a+aindex+n, v_a+bindex, Shift (n_b - n_a));
      }
      else {
        std::for_each (v_a+bindex+n, v_a+aindex, Shift (n_a - n_b));
      }
    }
    else {
      // Distinct container case.  Adjust indices after the swapped ranges.
      std::for_each (v_a+aindex+n, v_a+a.size_v(), Shift (n_b - n_a));
      std::for_each (v_b+bindex+n, v_b+b.size_v(), Shift (n_a - n_b));
    }
  }
}


/**
 * @brief Clear a range of elements within a vector.
 * @param auxid The aux data item being operated on.
 * @param dst Container holding the element
 * @param dst_index Index of the first element in the vector.
 * @param n Number of elements to clear.
 */
template <class T, class ALLOC>
void JaggedVecVectorFactory<T, ALLOC>::clear (SG::auxid_t auxid,
                                              AuxVectorData& dst,
                                              size_t dst_index,
                                              size_t n) const
{
  if (n == 0) return;

  vector_value_type* v = reinterpret_cast<vector_value_type*> (dst.getDataArray (auxid));
  IAuxTypeVector* lv = dst.getStore()->linkedVector (auxid);
  size_t begin = v[dst_index].begin(dst_index);
  size_t end = v[dst_index+n-1].end();
  size_t n_payload = end - begin;

  // Erase the payload elements.
  lv->shift (end, - n_payload);

  // Clear out the given range.
  Elt zero = Elt (begin);
  std::fill_n (v+dst_index, n, zero);

  // Adjust indices for following elements.
  std::for_each (v+dst_index+n, v+dst.size_v(), Shift (-n_payload));
}


} // namespace SG
