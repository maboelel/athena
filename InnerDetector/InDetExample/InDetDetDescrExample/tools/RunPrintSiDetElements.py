#!/usr/bin/env python
"""Run PrintSiDetectorElements

Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
"""
import sys
from argparse import ArgumentParser

from AthenaConfiguration.AllConfigFlags import initConfigFlags
from AthenaConfiguration.TestDefaults import defaultConditionsTags

# Argument parsing
parser = ArgumentParser("PrintSiDetectorElements.py")
parser.add_argument("detectors", metavar="detectors", type=str, nargs="*",
                    help="Specify the list of detectors")
parser.add_argument("--localgeo", default=False, action="store_true",
                    help="Use local geometry XML files")
parser.add_argument("--geometrytag",default="ATLAS-P2-RUN4-03-00-00", type=str,
                    help="The geometry tag to use")
parser.add_argument("--sqlitefile",default="", type=str,
                    help="SQLite input file to use")                    
args = parser.parse_args()


# Some info about the job
print("----PrintSiDetectorElements----")
print()
if args.localgeo:
    print("Using local Geometry XML files")
if not args.detectors:
    print("Running complete detector")
else:
    print("Running with: {}".format(", ".join(args.detectors)))
print()

# Configure
flags = initConfigFlags()
flags.Concurrency.NumThreads = 1
if flags.Concurrency.NumThreads > 0:
    flags.Scheduler.ShowDataDeps = True
    flags.Scheduler.ShowDataFlow = True
    flags.Scheduler.ShowControlFlow = True

flags.GeoModel.Align.Dynamic = False
flags.GeoModel.AtlasVersion = args.geometrytag
flags.Input.isMC = True
flags.IOVDb.GlobalTag = defaultConditionsTags.RUN4_MC
flags.Input.Files = []

if args.localgeo:
    flags.ITk.Geometry.AllLocal = True

elif args.sqlitefile:
    print("Using SQLite input")
    flags.GeoModel.SQLiteDB = True
    flags.GeoModel.SQLiteDBFullPath = args.sqlitefile

from AthenaConfiguration.DetectorConfigFlags import setupDetectorFlags
setupDetectorFlags(flags, args.detectors, toggle_geometry=True)

flags.lock()

# Construct our accumulator to run
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
acc = MainServicesCfg(flags)
from AthenaConfiguration.ComponentFactory import CompFactory

# ITk Pixel
if flags.Detector.EnableITkPixel:
    from PixelGeoModelXml.ITkPixelGeoModelConfig import ITkPixelReadoutGeometryCfg
    acc.merge(ITkPixelReadoutGeometryCfg(flags))

    ReadPixelDetElements = CompFactory.ReadSiDetectorElements('ReadPixelDetElements')
    ReadPixelDetElements.ManagerName = "ITkPixel"
    ReadPixelDetElements.DetEleCollKey = "ITkPixelDetectorElementCollection"
    ReadPixelDetElements.UseConditionsTools = False
    acc.addEventAlgo(ReadPixelDetElements)

    PrintPixelDetElements = CompFactory.PrintSiElements('PrintPixelDetElements')
    PrintPixelDetElements.OutputLevel = 5
    PrintPixelDetElements.DetectorManagerNames = ["ITkPixel"]
    PrintPixelDetElements.OutputFile = "PixelGeometry.dat"
    acc.addEventAlgo(PrintPixelDetElements)


# ITk Strip
if flags.Detector.EnableITkStrip:
    from StripGeoModelXml.ITkStripGeoModelConfig import ITkStripReadoutGeometryCfg
    acc.merge(ITkStripReadoutGeometryCfg(flags))

    ReadStripDetElements = CompFactory.ReadSiDetectorElements('ReadStripDetElements')
    ReadStripDetElements.ManagerName = "ITkStrip"
    ReadStripDetElements.DetEleCollKey = "ITkStripDetectorElementCollection"
    ReadStripDetElements.UseConditionsTools = False
    acc.addEventAlgo(ReadStripDetElements)

    PrintStripDetElements = CompFactory.PrintSiElements('PrintStripDetElements')
    PrintStripDetElements.OutputLevel = 5
    PrintStripDetElements.DetectorManagerNames = ["ITkStrip"]
    PrintStripDetElements.OutputFile = "StripGeometry.dat"
    acc.addEventAlgo(PrintStripDetElements)

# Execute and finish
sc = acc.run(maxEvents=1)

# Success should be 0
sys.exit(not sc.isSuccess())
